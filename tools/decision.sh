#!/bin/bash

# Load roles data dynamically
roles_data=$(cat ./tools/roles.json)

# Define user and resource
user='admin'
resource='read'

# Create a temporary JSON file with input data
echo "{\"user\": {\"role\": \"$user\"}, \"resource\": \"$resource\"}" > /tmp/input.json

# Evaluate the policy using OPA
decision=$(opa eval -i /tmp/input.json -d ./tools/example.rego "data.main.allow")

# Print the decision
echo "User '$user' can $([ "$decision" == "true" ] && echo "access" || echo "not access") the resource '$resource'."

# Remove the temporary JSON file
rm /tmp/input.json
