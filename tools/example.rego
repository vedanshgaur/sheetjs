package main

default allow = false

allow {
    trace(sprintf("Input: %v", input))
    input.user.role == role
    trace(sprintf("Role: %v", role))
    data.roles[_].permissions[_] == input.resource
}
